// Copyright 2023 Guo Tingjin(ericple)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import AbilitySkill from "./skill";

class HapExtAbility {
    description: string = '';
    descriptionId: number = 0;
    icon: string = '';
    iconId: number = 0;
    label: string = '';
    labelId: number = 0;
    name: string = '';
    priority: number = 0;
    skills: AbilitySkill = new AbilitySkill;
    srcEntrance: string = '';
    type: string = '';
    visible: boolean = false;
}

export default HapExtAbility;