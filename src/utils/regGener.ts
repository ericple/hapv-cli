// Copyright 2023 Guo Tingjin(ericple)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
export function appNameRegex(labelKey: string, descKey: string): RegExp {
    return new RegExp(`00.{2}000000.{2}000000.{2}0000.{2}.{2}00(.*?)00.{2}00${Buffer.from(labelKey).toString('hex')}`,'g');
}

export function appNameFilterRegex(labelKey: string): RegExp {
    return new RegExp(`00.{2}000000.{2}000000.{2}0000.{2}.{2}00(.*?)00.{2}00`, 'g');
}