function help() {
    console.log('Usage: hapv --in [file] [options]\n')
    console.log('Options:')
    console.log('\t--in\t\t\tspecify the hap file waiting for unpack')
    console.log('\t--out [path]\t\twrite hap information to a file')
    console.log('\t--h, --help\t\tshow this help text')
    console.log('\t--debug\t\t\tprogressivly show unpack process')
    console.log('\t--get-icon\t\tget app icon to the working directory')
    console.log('\t--info\t\t\tprint app info into the terminal')
    console.log('\t--save-cache\t\tsave cache after hap file process finished.')
    console.log('\nDocumentation can be found at https://gitee.com/ericple/hapv-cli')
}

export default help;