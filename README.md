# HapvCli

HapvCli is a command-line tool for developers to unpack `.hap` package / Fast signing / install

## Installation Guide

- You can download this project via npm or any other package managment tool you like, use `npm` as an example, just type the command below to your terminal:

```bash
npm install -g @ohosdev/hapv
```

- You can also download / clone this repository, modify it or not (at your will), then follow the instructions below:

```bash
cd "to your project root directory"

npm install -g .
```

## Usage

### - Analyse hap permission

```bash
hapv --in "path to your hap file"
```

output:

```
Reading resources buffer...
Reading modules information...
Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.GET_BUNDLE_INFO_PRIVILEGED

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.INSTALL_BUNDLE

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.LISTEN_BUNDLE_CHANGE

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.MANAGE_MISSIONS

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.REQUIRE_FORM

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.INPUT_MONITORING

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.NOTIFICATION_CONTROLLER

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.MANAGE_SECURE_SETTINGS

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.START_ABILITIES_FROM_BACKGROUND

```

### - Check hap package information

```bash
hapv --in "path to your hap file" --info
```

output:

```
Reading resources buffer...
Reading modules information...
OutInfo {
  appName: '设备信息',
  bundleName: 'org.ohosdev.deviceinfo',
  versionName: '1.2.4',
  versionNumber: 1000006,
  compileTarget: 9,
  vendor: 'ohosdev',
  requestPermissionNumber: 1,
  requestedPermissions: [ { name: 'ohos.permission.sec.ACCESS_UDID' } ]
}
```

### - Get application icon from hap

```bash
hapv --in "path to your hap file" --get-icon
```

## License

- This project is open under Apache2.0 License
- Used package:
  - compressing `MIT license`

- Reference Project

  > Referrence for fetching app name from hap package
  - [@ohos-dev/hap-viewer](https://gitee.com/ohos-dev/hap-viewer) `Apache-2.0`