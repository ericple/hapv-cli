import requestPermission from "../type/requestPermissions";

function analysePermission(permissions: requestPermission[]): number[] {
    let index = 0;
    let result: number[] = [];
    while(index < permissions.length) {
        let permission = permissions[index];
        if(!permission.reason && !permission.usedScene) {
            console.warn(`Suspicious permission request: Reason and usedScene not provided`);
            console.warn(`Target permission: ${permission.name}\n`);
            result.push(-1);
        }else if(!permission.reason) {
            console.warn(`Suspicious permission request: Reason not provided`);
            console.warn(`Target permission: ${permission.name}\n`);
            result.push(-2);
        }else if(!permission.usedScene) {
            console.warn(`Suspicious permission request: UsedScene not provided`);
            console.warn(`Target permission: ${permission.name}\n`);
            result.push(-3);
        }
        index++;
    }
    return result;
}

export default analysePermission;