// Copyright 2023 Guo Tingjin(ericple)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import path from 'path';
import fs from 'fs';
function checkPath(src: string) {
    if(path.isAbsolute(src)) {
        return fs.existsSync(src) && src;
    }else{
        return fs.existsSync(path.join(__dirname, src)) && path.join(__dirname, src);
    }
}

export default checkPath;