// Copyright 2023 Guo Tingjin(ericple)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
class AppInfo {
    apiReleaseType: string = '';
    bundleName: string = '';
    compileSdkType: string = '';
    compileSdkVersion: string = '';
    icon: string = '';
    iconId: number = 0;
    label: string = '';
    labelId: number = 0;
    minAPIVersion: number = 0;
    targetAPIVersion: number = 0;
    vendor: string = '';
    versionCode: number = 0;
    versionName: string = '';

    public getIconKey() {
        const descArr = this.icon.split(':');
        let result = '';
        if(descArr.length == 1) {
            result = this.icon;
        }else{
            result = descArr[1];
        }
        return result;
    }

    public getLabelKey() {
        const labelArr = this.label.split(':');
        let result = '';
        if(labelArr.length == 1) {
            result = this.label;
        }else{
            result = labelArr[1];
        }
        return result;
    }
}

export default AppInfo;