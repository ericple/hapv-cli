# HapvCli

HapvCli 为开发者提供对`.hap`文件的快速解包/安装/签名功能

## 安装指南

- 你可以使用npm或其它任何你习惯的node包管理器获取本项目, 以 `npm` 为例, 只需在终端中输入以下命令:

```bash
npm install -g @ohosdev/hapv
```

- 你也可以下载或克隆本仓库，随后按自己意愿修改后，根据以下指南从本地安装:

```bash
cd "to your project root directory"

npm install -g .
```

## 使用

### - hap包权限分析

```bash
hapv --in "path to your hap file"
```

output:

```
Reading resources buffer...
Reading modules information...
Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.GET_BUNDLE_INFO_PRIVILEGED

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.INSTALL_BUNDLE

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.LISTEN_BUNDLE_CHANGE

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.MANAGE_MISSIONS

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.REQUIRE_FORM

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.INPUT_MONITORING

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.NOTIFICATION_CONTROLLER

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.MANAGE_SECURE_SETTINGS

Suspicious permission request: Reason and usedScene not provided
Target permission: ohos.permission.START_ABILITIES_FROM_BACKGROUND

```

### - 获取hap包基本信息

```bash
hapv --in "path to your hap file" --info
```

输出:

```
Reading resources buffer...
Reading modules information...
OutInfo {
  appName: '设备信息',
  bundleName: 'org.ohosdev.deviceinfo',
  versionName: '1.2.4',
  versionNumber: 1000006,
  compileTarget: 9,
  vendor: 'ohosdev',
  requestPermissionNumber: 1,
  requestedPermissions: [ { name: 'ohos.permission.sec.ACCESS_UDID' } ]
}
```

### - 获取应用图标

```bash
hapv --in "path to your hap file" --get-icon
```

## 许可

- 本项目以 Apache0-2.0 许可证开源
- 使用的包:
  - `compressing` - `MIT license`

- 参考项目
  > 本项目解析应用名称的代码逻辑来源
  - [@ohos-dev/hap-viewer](https://gitee.com/ohos-dev/hap-viewer) `Apache-2.0`