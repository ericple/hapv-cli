// Copyright 2023 Guo Tingjin(ericple)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
const HAPV_CONSTANTS = {
    ACCEPTABLE_ARGUMENTS: [
        '--in',
        '--out',
        '--work-dir',
        '--help',
        '--h',
        '--replace-app-icon',
        '--debug',
        '--get-icon',
        '--info',
        '--save-cache'
    ],
    NECESSARY_ARGUMENTS: [
        '--in'
    ],
    NAMED_ARGUMENTS: {
        IN_FILE: '--in',
        WORK_DIR: '--work-dir',
        SHOW_INFO: '--info',
        DEBUG_FLAG: '--debug',
        GET_ICON: '--get-icon',
        SAVE_CACHE: '--save-cache',
        OUTPUT: '--out',
        _HELP: '--h',
        HELP: '--help'
    }
}

export default HAPV_CONSTANTS;